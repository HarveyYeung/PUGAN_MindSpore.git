import mindspore as ms
from mindspore import nn
from .commons import UNetUp, UNetDown, Fusion
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from skimage.morphology import disk
import skimage.filters.rank as sfr
from mindspore.ops import operations as P
import mindspore.ops as ops
def inv(x , t):
    cat = P.Concat(axis=0)
    unsqueeze = ops.ExpandDims()
    fx = []
    a = []
    t = 1.0 / t
    for j in range(x.shape[0]):
        for i in range(3):
            fx.append(x[j][i] * t[j][0])
        a.append(unsqueeze(cat([unsqueeze(fx[0],0), unsqueeze(fx[1],0), unsqueeze(fx[2],0)]),0))
    J = cat(a)
    return J

class GeneratorUNet(nn.Cell):
    def __init__(self, in_channels=3, out_channels=3):
        super(GeneratorUNet, self).__init__()
        self.down1 = UNetDown(in_channels, 64, normalize=False)
        self.down2 = UNetDown(64, 128)
        self.down3 = UNetDown(128, 256)
        self.down4 = UNetDown(256, 512, dropout=0.5)
        self.down5 = UNetDown(512, 512, dropout=0.5)

        self.down11 = UNetDown(3, 64, normalize=False)
        self.down22 = UNetDown(64, 128)
        self.down33 = UNetDown(128, 256)
        self.down44 = UNetDown(256, 512, dropout=0.5)

        self.up4 = UNetUp(512, 512, dropout=0.5)
        self.up3 = UNetUp(512, 256)
        self.up2 = UNetUp(256, 128)
        self.up1 = UNetUp(128, 64)

        self.fs4 = Fusion(512, 512)
        self.fs3 = Fusion(256, 256)
        self.fs2 = Fusion(128, 128)
        self.fs1 = Fusion(64, 64)

        self.pool = nn.MaxPool2d(3, stride = 2, padding = 1, pad_mode='pad')

        self.contr5 = nn.SequentialCell(
            nn.Conv2d(1, 3, 3, padding=1, pad_mode='pad'),
            nn.LeakyReLU(),
            nn.Conv2d(3, 1, 3, padding=1, pad_mode='pad'),
            nn.Tanh()
        )
        self.Upsample = nn.ResizeBilinear(half_pixel_centers=True)

           
        self.final = nn.SequentialCell(
            nn.Upsample(scale_factor=(2.0,2.0), mode='bilinear',recompute_scale_factor=True),
            nn.ZeroPad2d((1, 0, 1, 0)),
            nn.Conv2d(64, out_channels, 4, padding=1, pad_mode='pad'),
            nn.Tanh()
        )

    def construct(self, x, j):
        #print("(x.shape,j.shape)")
        #print(x.shape,j.shape)
        J = inv(x,j)
        #print("J.shape")
        #print(J.shape)
        j1 = self.pool(j)
        #print("j1.shape")
        #print(j1.shape)
        j2 = self.pool(j1)
        #print("j2.shape")
        #print(j2.shape)
        j3 = self.pool(j2)
        #print("j3.shape")
        #print(j3.shape)
        j4 = self.pool(j3)
        #print("j4.shape")
        #print(j4.shape)
        j5 = self.pool(j4)
        #print("j5.shape")
        #print(j5.shape)
        #3-64
        d1 = self.down1(x)
        #print("d1.shape")
        #print(d1.shape)
        dj1 = self.down11(J)
        #print("dj1.shape")
        #print(dj1.shape)
        #64-128
        d2 = self.down2(d1)
        dj2 = self.down22(dj1)
        #print("d2.shape")
        #print(dj2.shape)
        #128-256
        d3 = self.down3(d2)
        dj3 = self.down33(dj2)
        #print("d3.shape")
        #print(d3.shape)
        #256-512
        d4 = self.down4(d3)
        dj4 = self.down44(dj3)
        #print("d4.shape")
        #print(dj4.shape)
        #512-512
        d5 = self.down5(d4)
        #print("d5.shape")
        #print(d5.shape)
        d5 = d5 * self.contr5(j5)
        #print("d52.shape")
        #print(d5.shape)
        # 512-512

        # (32, 512, 14, 14) (32, 512, 14, 14) (32, 1, 16, 16)
        d4 = self.fs4(d4, dj4, j4)
        #print(" d5.shape,d4.shape")
        #print(d5.shape,d4.shape)
        u4 = self.up4(d5, d4)
        #print(" d3 = self.fs3(d3, dj3, j3)")
        #print(d3.shape,dj3.shape,j3.shape)
        #512-256
        d3 = self.fs3(d3, dj3, j3)
        u3 = self.up3(u4, d3)

        #256-128
        d2 = self.fs2(d2, dj2, j2)
        u2 = self.up2(u3, d2)

        #128-64
        d1 = self.fs1(d1, dj1, j1)
        u1 = self.up1(u2, d1)
        # u1 =  self.Upsample(u1, (2, 2))  
        return self.final(u1)


