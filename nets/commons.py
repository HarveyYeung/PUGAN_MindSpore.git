import os
import cv2
import time
import mindspore
import mindspore.nn as nn

import numpy as np
import matplotlib.pyplot as plt
from mindspore.ops import operations as P
from mindspore.ops import functional as F
import mindspore.ops as ops
from mindspore import Parameter, Tensor
from mindspore.common.initializer import initializer
def Weights_Normal(m):
    # initialize weights as Normal(mean, std)
    classname = m.__class__.__name__
    if classname.find("Conv") != -1:
        nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find("BatchNorm2d") != -1:
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        nn.init.constant_(m.bias.data, 0.0)

class Fusion(nn.Cell):
    def __init__(self, in_channels, out_channels):
        super(Fusion, self).__init__()
        self.attention = nn.SequentialCell(
            nn.Conv2d(1, 3, 3, padding=1, pad_mode='pad'),
            nn.LeakyReLU(),
            nn.Conv2d(3, 1, 3, padding=1, pad_mode='pad'),
            nn.Tanh()
        )   

        self.cov1 = nn.SequentialCell(
            nn.Conv2d(in_channels, 64, 3, 1, padding=1, pad_mode='pad'),
            nn.ReLU(),
            nn.Conv2d(64, 1, 3, padding=1, pad_mode='pad'),
            nn.Tanh()
        )
        self.cov2 = nn.SequentialCell(
            nn.Conv2d(in_channels, out_channels, 3, 1,'pad', 1),
            nn.ReLU()
        )
        self.cov3 = nn.SequentialCell(
            nn.Conv2d(in_channels, out_channels, 3, 1,'pad', 1),
            nn.ReLU()
        )
        self.Norm = nn.InstanceNorm2d(1)
        self.Tan = nn.Tanh()
        self.less_equal = P.LessEqual()
    def construct(self, F1, F2, A):
        A = self.Norm(self.attention(A))
        dif = self.Norm(self.cov1(F2 - F1))
        A = self.less_equal(dif, A) * A +self.less_equal(A, dif) * dif
        A = self.Tan(A)
        A = F1 * A
        A = self.cov2(A)
        A = self.Tan(A)
        return A

class ConLayer(nn.Cell):
    def __init__(self, in_channels, out_channels, kernel_size, stride):
        super(ConLayer, self).__init__()
        reflection_padding = kernel_size // 2
        self.reflection_pad = nn.ReflectionPad2d(reflection_padding)#       
        self.conv2d = nn.Conv2d(in_channels, out_channels, kernel_size, stride, pad_mode='valid')

    def construct(self, x):
        out = self.reflection_pad(x)
        # print("reflection_pad.shape")
        # print(out.shape)
        out = self.conv2d(out)
        return out

class ResidualBlock(nn.Cell):
    def __init__(self, in_channels, out_channels):
        super(ResidualBlock, self).__init__()
        self.conv1 = ConLayer(in_channels, in_channels, kernel_size=3, stride=1)
        self.conv2 = ConLayer(in_channels, in_channels, kernel_size=3, stride=1)
        self.conv3 = ConLayer(in_channels, out_channels, kernel_size=3, stride=1)
        self.relu1 = nn.PReLU()
        self.relu2 = nn.PReLU()
        self.add = P.Add()
    def construct(self, x):
        residual = x
        # print("residual.shape")
        # print(residual.shape)
        out = self.relu1(self.conv1(x))
        # print("out.shape")
        # print(out.shape)
        out = self.relu2(self.conv2(out) * 0.1)
        # print("out.shape")
        # print(out.shape)
        out = self.add(out, residual)

        out = self.conv3(out)
        return out

class UNetDown(nn.Cell):
    """ Standard UNet down-sampling block 
    """
    def __init__(self, in_size, out_size, normalize=True, dropout=0.0):
        super(UNetDown, self).__init__()
        # layers = [nn.Conv2d(in_size, out_size, 4, 2, 'pad', has_bias=False)]
        layers = [nn.Conv2d(in_size, out_size, 4, 2,padding=1,pad_mode='pad', has_bias=False)]
        
        if normalize:
            layers.append(nn.InstanceNorm2d(out_size))
        layers.append(nn.LeakyReLU(0.2))
        if dropout:
            layers.append(nn.Dropout(dropout))
        self.model = nn.SequentialCell(*layers)
        self.dense = nn.SequentialCell(
            ResidualBlock(out_size, out_size),
            ResidualBlock(out_size, out_size),
            ResidualBlock(out_size, out_size)
        )

    def construct(self, x):
        x = self.model(x)
        return x


class UNetUp(nn.Cell):
    def __init__(self, in_size, out_size, dropout=0.0):
        super(UNetUp, self).__init__()
        # self.Upsample = nn.ResizeBilinear(half_pixel_centers=True)

        layers = [
            nn.Upsample(scale_factor=(2.0,2.0), mode='bilinear',recompute_scale_factor=True),
            nn.Conv2d(in_size, out_size, 3, 1,padding=1,pad_mode='pad', has_bias=False),
            nn.InstanceNorm2d(out_size),
            nn.ReLU(),
        ]
        if dropout:
            layers.append(nn.Dropout(dropout))
        self.model = nn.SequentialCell(*layers)
        self.dense = nn.SequentialCell(
            ResidualBlock(out_size * 2, out_size * 2),
            ResidualBlock(out_size * 2, out_size),
            ResidualBlock(out_size, out_size)
        )
        self.cat = P.Concat(axis=1)

    def construct(self, x, skip_input):
        # x =  self.Upsample(x, (2, 2))  
        # x = F.interpolate(x, scales = (1.,1.,2.,2.), mode='bilinear')
        # print(x.shape)
        x = self.model(x)
        # print("x.shape,skip_input.shape")
        # print(x.shape,skip_input.shape)
        x1 = self.cat((x, skip_input))
        x = self.dense(x1)
        return x




class Net(nn.Cell):
    def __init__(self):
        super(Net, self).__init__()
        self.fill = ops.Fill()
    def construct(self, x):
        out = Parameter(self.fill(self.Tensor(initializer(1.0, d_interpolates.shape)),requires_grad=False))
        return out

class GradNetWrtX(nn.Cell):
    def __init__(self, net):
        super(GradNetWrtX, self).__init__()
        self.net = net
        self.grad_op = ops.GradOperation()
    def construct(self, x):
        gradient_function = self.grad_op(self.net)
        return gradient_function(x, y)



class Gradient_Penalty(nn.Cell):
    """ Calculates the gradient penalty loss for WGAN GP
    """
    def __init__(self, cuda=True):
        super(Gradient_Penalty, self).__init__()
        self.Tensor = Tensor
        self.gradNetWrtX = GradNetWrtX(Net())
        self.fill = ops.Fill()
        # output = fill(mindspore.float32, (2, 2), 1)
    def construct(self, D, real, fake):
        # Random weight term for interpolation between real and fake samples
        eps = self.Tensor(np.random.random((real.size(0), 1, 1, 1)))
        # Get random interpolation between real and fake samples
        interpolates = (eps * real + ((1 - eps) * fake)).requires_grad_(True)
        d_interpolates = D(interpolates)
        # fake = autograd.Variable(self.Tensor(d_interpolates.shape).fill_(1.0), requires_grad=False)
# init.initializer(init.Normal(1.0), d_interpolates.shape)
        fake = Parameter(self.fill(self.Tensor(d_interpolates.shape),(1.0)),requires_grad=False)
        # Get gradient w.r.t. interpolates
        output = gradNetWrtX(x, y)

        gradients = autograd.grad(outputs=d_interpolates,
                                  inputs=interpolates,
                                  grad_outputs=fake,
                                  create_graph=True,
                                  retain_graph=True,
                                  only_inputs=True,)[0]
        gradients = gradients.view(gradients.size(0), -1)
        gradient_penalty = ((gradients.norm(2, dim=1) - 1) ** 2).mean()
        return gradient_penalty



