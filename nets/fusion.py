import mindspore.nn as nn
import mindspore
from .pix2pix import GeneratorUNet
from mindspore.common.tensor import Tensor
# from mindspore.ops.function as F
from mindspore.ops import functional as F
from mindspore.ops import operations as P
import mindspore.ops as ops
class Discriminator(nn.Cell):
    def __init__(self, in_channels=3):
        super(Discriminator, self).__init__()

        def discriminator_block(in_filters, out_filters, normalization=True):
            """Returns downsampling layers of each discriminator block"""
            layers = [nn.Conv2d(in_filters, out_filters, 4, stride=2,  pad_mode='pad', padding=1)]
            if normalization:
                layers.append(nn.BatchNorm2d(out_filters))
            layers.append(nn.LeakyReLU(0.2))
            return layers
        main = nn.SequentialCell()
        layers = discriminator_block(in_channels, 64, normalization=False)
        for layer in layers:
            main.append(layer)
        layers = discriminator_block(64, 128, normalization=False)
        for layer in layers:
            main.append(layer)
        layers = discriminator_block(128, 256, normalization=False)
        for layer in layers:
            main.append(layer)
        layers = discriminator_block(256, 512, normalization=False)
        for layer in layers:
            main.append(layer)
        # main.append(nn.ZeroPad2d((1, 0, 1, 0)))
        main.append(nn.Conv2d(512, 1, 4, padding=1, pad_mode='pad',  has_bias=False))
        self.model = main
        # nn.SequentialCell(
        #     *discriminator_block(in_channels, 64, normalization=False),
        #     *discriminator_block(64, 128, normalization=False),
        #     *discriminator_block(128, 256, normalization=False),
        #     *discriminator_block(256, 512, normalization=False),
        #     nn.ZeroPad2d((1, 0, 1, 0)),
        #     nn.Conv2d(512, 1, 4, padding=1, pad_mode='pad',  has_bias=False)
        # )

    def construct(self, img):
        #print('self.model(img)')
        #print(img.shape)
        return self.model(img)

class Discriminator1(nn.Cell):
    def __init__(self, in_channels=4):
        super(Discriminator1, self).__init__()

        def discriminator_block(in_filters, out_filters, normalization=True):
            """Returns downsampling layers of each discriminator block"""
            layers = [nn.Conv2d(in_filters, out_filters, 4, pad_mode='pad', padding=1)]
            if normalization:
                layers.append(nn.InstanceNorm2d(out_filters))
            layers.append(nn.LeakyReLU(0.2))
            return layers

        self.model = nn.SequentialCell(
            *discriminator_block(in_channels, 64, normalization=False),
            *discriminator_block(64, 128, normalization=False),
            *discriminator_block(128, 256, normalization=False),
            *discriminator_block(256, 512, normalization=False),
            # nn.ZeroPad2d((1, 0, 1, 0)),
            nn.Conv2d(512, 1, 4, padding=1, pad_mode='pad',  has_bias=False)
        )

    def construct(self, img):
        return self.model(img)

class PUGAN:
    def __init__(self):
        self.netG = GeneratorUNet()
        self.netD = Discriminator()
        self.netD1 = Discriminator1()


class Gradient_Difference_Loss(nn.Cell):
    def __init__(self, alpha=1, chans=3, cuda=True):
        super(Gradient_Difference_Loss, self).__init__()
        self.alpha = alpha
        self.chans = chans
        SobelX = [[1, 2, 1], [0, 0, 0], [-1, -2, -1]]
        SobelY = [[1, 2, -1], [0, 0, 0], [1, 2, -1]]
        # self.Kx = Tensor(SobelX).expand(self.chans, 1, 3, 3)
        # self.Ky = Tensor(SobelY).expand(self.chans, 1, 3, 3)
        self.Kx = F.broadcast_to(Tensor(SobelX,mindspore.float32),(self.chans, 1, 3, 3))
        self.Ky = F.broadcast_to(Tensor(SobelY,mindspore.float32),(self.chans, 1, 3, 3))
        self.mean = P.ReduceMean(keep_dims=True)
    def get_gradients(self, im):
        # #print(type(batch_x))
        #print("im.dtype:",im.dtype,self.Kx.dtype,self.Ky.dtype)
        gx = ops.conv2d(im, self.Kx, stride=1, padding=1,pad_mode='pad', groups=self.chans)
        gy = ops.conv2d(im, self.Ky, stride=1, padding=1,pad_mode='pad', groups=self.chans)
        return gx, gy

    def construct(self, pred, true):
        # get graduent of pred and true
        gradX_true, gradY_true = self.get_gradients(true)
        grad_true = ops.abs(gradX_true) + ops.abs(gradY_true)
        gradX_pred, gradY_pred = self.get_gradients(pred)
        #print("================== gradX_pred, gradY_pred================================")
        #print(gradX_pred.shape, gradY_pred.shape)
        grad_pred_a = ops.abs(gradX_pred)**self.alpha + ops.abs(gradY_pred)**self.alpha
        # compute and return GDL
        #print(grad_true.shape, grad_pred_a.shape)
        return 0.5 * self.mean(grad_true - grad_pred_a)

