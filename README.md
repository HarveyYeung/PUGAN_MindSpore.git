# PUGAN_MindSpore_TIP2023

Runmin Cong, Wenyu Yang, Wei Zhang, Chongyi Li, Chun-Le Guo, Qingming Huang, and Sam Kwong, PUGAN: Physical Model-Guided Underwater Image Enhancement Using GAN with Dual-Discriminators, IEEE TRANSACTIONS ON IMAGE PROCESSING, 2023.

## Network

#### Our overall framework:

![image](figures/overall.jpg)

#### Par-subnet:

![image](figures/Par-subnet.jpg)

#### TSIE-subnet:

![image](figures/TSIE-subnet.jpg)

#### Requirement:

Pleasure configure the environment according to the given version:

- python 3.8
- mindspore-gpu 2.0.0
- GPU cuda 11.1
- pillow 9.5.0
- skimage 0.21.0
- numpy  1.24.3
 

## Data Preprocessing

 Please follow the tips to download the processed datasets and pre-trained model:

  1. Download training data  from [[Link](https://pan.baidu.com/s/11PGupIgdfN506AYC6jK1ew?pwd=mvpP)], code: mvpP. 
 2. Download testing data from [[Link](https://pan.baidu.com/s/1kNTtddujLjv6KU6BPyUFYA?pwd=mvpP)], code: mvpP.


```python
├── utils
    ├── data_utils.py
├── Par
    ├── model
    ├── model.py
├── nets
    ├── pixpix.py
    ├── fusion.py
    ├── commons.py
├── checkpoints
├── test.py
├── train.py
```


## Training and Testing

**Training command** :
Please unzip the training data set to data\input_train and unzip the corresponding reference of training data set to data\gt_train. 

We provide "train.yaml" files for training a new model from scratch or from a existing model.

```python
python train.py
```

You can also train on a UFO or EVUP dataset by modifying train.yaml. We provide download connections for these datasets : UFO: [[link](https://irvlab.cs.umn.edu/resources/ufo-120-dataset)], EUVP:[[link](http://irvlab.cs.umn.edu/resources/euvp-dataset)]

**Testing command** :
Please unzip the testing data set to tests. 

We provide "test.yaml" files for testing.

The trained model can be download here: [[Link]n(https://pan.baidu.com/s/1y0_kHl1NRjrKc36LEX9wFQ?pwd=mvpP)], code: .mvpP

```python
python test.py
```


## Bibtex

```
   @article{PUGAN,
     title={PUGAN: Physical model-guided underwater image enhancement using GAN with dual-discriminators},
     author={Cong, Runmin and Yang, Wenyu and Zhang, Wei and Li, Chongyi and Guo, Chun-Le and Huang, Qingming and Kwong, Sam},
     journal={IEEE Trans. Image Process. },
     year={},
     publisher={IEEE}
    }
  
```

## Contact Us

If you have any questions, please contact Runmin Cong at [rmcong@sdu.edu.cn](mailto:rmcong@sdu.edu.cn) or Qi Qin at [wyuyang@bjtu.edu.cn](mailto:wyuyang@bjtu.edu.cn).