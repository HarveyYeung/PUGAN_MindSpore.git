import os
import sys
import yaml
import argparse
import numpy as np
from PIL import Image
from nets.fusion import PUGAN, Gradient_Difference_Loss
from nets.commons import Weights_Normal, Gradient_Penalty
from utils.data_utils import GetTrainingPairs, GetValImage,get_train_loader
import matplotlib.pyplot as plt
from Par.model import DNet, TtoDNet, AENet
from math import exp
import mindspore.dataset.vision as vision
from mindspore.dataset.transforms import Compose
import mindspore as ms
from mindspore import Tensor
import mindspore.nn as nn
import mindspore.ops as ops
from mindspore.common import initializer as init
import mindspore.common.dtype as mstype
from mindspore import context
from mindspore.train.serialization import load_checkpoint, load_param_into_net, save_checkpoint
import mindspore.numpy as mnp
from mindspore.ops import operations as P
## get configs and training options
parser = argparse.ArgumentParser()
parser.add_argument("--cfg_file", type=str, default="train.yaml")
args = parser.parse_args()
with open(args.cfg_file) as f:
    cfg = yaml.load(f, Loader=yaml.FullLoader)
# get info from config file
dataset_name = cfg["dataset_name"]
dataset_path = cfg["dataset_path"]
channels = cfg["chans"]
img_width = cfg["im_width"]
img_height = cfg["im_height"]
val_interval = cfg["val_interval"]
ckpt_interval = cfg["ckpt_interval"]
from_epoch = cfg["from_epoch"]
num_epochs = cfg["num_epochs"]
batch_size = cfg["batch_size"]
lr_rate = cfg["lr_rate"]
num_critic = cfg["num_critic"]
lambda_gp = cfg["gp_weight"]
lambda_gp1 = cfg["gp1_weight"]
lambda_1 = cfg["l1_weight"]
lambda_2 = cfg["lg_weight"]
lambda_d = cfg["lambda_d"]
lambda_d1 = cfg["lambda_d1"]
model_path_d = cfg["path_d"]
model_path_t = cfg["path_t"]
model_path_a = cfg["path_a"]
# model_path_g_from = cfg["path_g"]
# model_path_d_from = cfg["path_D"]
# model_path_d1_from = cfg["path_D1"]
version = cfg["version"]

def init_weight(net):
    for _, cell in net.cells_and_names():
        if isinstance(cell, (nn.Conv2d, nn.Conv2dTranspose)):
            cell.weight.set_data(init.initializer(init.Normal(0.02), cell.weight.shape))
        elif isinstance(cell, nn.BatchNorm2d):
            cell.gamma.set_data(init.initializer(Tensor(np.random.normal(1, 0.02, cell.gamma.shape), \
            mstype.float32), cell.gamma.shape))
            cell.beta.set_data(init.initializer('zeros', cell.beta.shape))
        elif isinstance(cell, nn.Dense):
            cell.weight.set_data(init.initializer(init.Normal(0.02), cell.weight.shape))


class GenWithLossCell(nn.Cell):
    """Generator with loss(wrapped)"""

    def __init__(self, netG, netD, netD1, Dnet,L_gdl):
        super(GenWithLossCell, self).__init__()
        self.netG = netG
        self.netD = netD
        self.netD1 = netD1
        self.Dnet = Dnet
        self.L1_G = nn.L1Loss()
        self.L_gdl = L_gdl
        self.cat = P.Concat(axis=1)
    def construct(self, imgs_distorted, tm, imgs_good_gt):

        imgs_fake = self.netG(imgs_distorted, tm)
        #           pred_fake = discriminator(imgs_fake.detach())   
        pred_fake = self.netD(imgs_fake)
        loss_gen = -pred_fake.mean()
        depth_fake = ops.stop_gradient(self.cat([self.Dnet(imgs_fake), imgs_fake]))
        pred_fake1 = self.netD1(depth_fake)
        loss_gen1 = -pred_fake1.mean()
        # print("=================imgs_fake.shape,imgs_good_gt.shape===========================")
        # print(imgs_fake.shape,imgs_good_gt.shape)
        loss_1 = self.L1_G(imgs_fake, imgs_good_gt)
   
        loss_gdl =  self.L_gdl(imgs_fake, imgs_good_gt)
        loss_G = lambda_d * loss_gen + lambda_d1 * loss_gen1 + lambda_1 * loss_1.mean() + lambda_2 * loss_gdl.mean()
        # print("=================loss_G.shape===========================")
        # print(loss_G)
        return loss_G

class DisWithLossCell(nn.Cell):
    """ Discriminator with loss(wrapped) """

    def __init__(self, netG, netD):
        super(DisWithLossCell, self).__init__()
        self.netG = netG
        self.netD = netD
        self.gradop = ops.GradOperation()
        self.LAMBDA = lambda_gp #100
        self.uniform = ops.UniformReal()


    def compute_gradient_penalty(self, real_samples, fake_samples):
        """Calculates the gradient penalty loss for WGAN GP"""

        # Get random interpolation between real and fake samples
        alpha = self.uniform((real_samples.shape[0], 1, 1, 1))
        interpolates = (alpha * real_samples + ((1 - alpha) * fake_samples))
        # print("==========================================interpolates.shape=====",interpolates.shape)
        # print(interpolates.shape)
        grad_fn = self.gradop(self.netD)
        gradients = grad_fn(interpolates)
        gradients = gradients.view(gradients.shape[0], -1)
        # gradient_penalty = ops.reduce_mean(((mnp.norm(gradients, 2, axis=1) - 1) ** 2))
        gradient_penalty = ((gradients.norm(2,  dim=1) - 1) ** 2).mean()
        return gradient_penalty.mean()

    def construct(self, imgs_distorted, tm, imgs_good_gt):

        errD_real = self.netD(imgs_good_gt)
        # print("==================================================")
        # print(imgs_distorted.shape, tm.shape)
        fake = self.netG(imgs_distorted, tm)
        fake = ops.stop_gradient(fake)
        errD_fake = self.netD(fake)
        gradient_penalty = self.compute_gradient_penalty(imgs_distorted, fake)
        return errD_fake.mean() - errD_real.mean() + gradient_penalty * self.LAMBDA


class DisWithLossCell1(nn.Cell):
    """ Discriminator with loss(wrapped) """
    def __init__(self, netG, netD, Dnet):
        super(DisWithLossCell1, self).__init__()
        self.netG = netG
        self.netD = netD
        self.Dnet = Dnet
        self.gradop = ops.GradOperation()
        self.LAMBDA = lambda_gp1 # 100 
        self.uniform = ops.UniformReal()
        self.cat = P.Concat(axis=1)
    def compute_gradient_penalty(self, real_samples, fake_samples):
        """Calculates the gradient penalty loss for WGAN GP"""

        # Get random interpolation between real and fake samples
        alpha = self.uniform((real_samples.shape[0], 1, 1, 1))
        interpolates = (alpha * real_samples + ((1 - alpha) * fake_samples))

        grad_fn = self.gradop(self.netD)
        gradients = grad_fn(interpolates)
        gradients = gradients.view(gradients.shape[0], -1)
        gradient_penalty = ops.reduce_mean(((mnp.norm(gradients, 2, axis=1) - 1) ** 2))
        return gradient_penalty

    def construct(self, imgs_distorted, tm, imgs_good_gt):
        depth_real = ops.stop_gradient(self.cat([self.Dnet(imgs_good_gt), imgs_good_gt]))
        fake = self.netG(imgs_distorted, tm)
        depth_fake = ops.stop_gradient(self.cat([self.Dnet(fake), fake]))
        errD_real = self.netD(depth_real)
        # fake = ops.stop_gradient(fake)
        errD_fake = self.netD(depth_fake)

        gradient_penalty = self.compute_gradient_penalty(depth_real, depth_fake)

        return errD_fake.mean() - errD_real.mean() + gradient_penalty * self.LAMBDA
 


## create dir for model
checkpoint_dir = "checkpoints/"
os.makedirs(checkpoint_dir, exist_ok=True)

L1_G = nn.L1Loss()  # l1 loss term
L1_gp = Gradient_Penalty()  # wgan_gp loss term
L_gdl = Gradient_Difference_Loss()  # GDL loss term

# Initialize generator and discriminator
ugan_ = PUGAN()
generator = ugan_.netG
discriminator = ugan_.netD
discriminator1 = ugan_.netD1

if (from_epoch == 0):
    init_weight(generator)
    init_weight(discriminator)
    init_weight(discriminator1)
# else:
#     ms.load_param_into_net(generator, ms.load_checkpoint(model_path_g_from))
#     ms.load_param_into_net(discriminator, ms.load_checkpoint(model_path_d_from))
#     ms.load_param_into_net(discriminator1, ms.load_checkpoint(model_path_d1_from))
Dnet = DNet()
Ttodnet = TtoDNet()
coenet = AENet()
ms.load_param_into_net(Dnet, ms.load_checkpoint(model_path_d))
ms.load_param_into_net(Ttodnet, ms.load_checkpoint(model_path_t))
ms.load_param_into_net(coenet, ms.load_checkpoint(model_path_a))


# # setup optimizer

optimizerG = nn.Adam(
    generator.trainable_params(),
    learning_rate=lr_rate)
optimizerD = nn.Adam(
    discriminator.trainable_params(),
    learning_rate=lr_rate)
optimizerD1 = nn.Adam(
    discriminator1.trainable_params(),
    learning_rate=lr_rate)

netG_train = nn.TrainOneStepCell(GenWithLossCell(generator, discriminator,discriminator1,Dnet,L_gdl), optimizerG)
netD_train = nn.TrainOneStepCell(DisWithLossCell(generator, discriminator), optimizerD)
netD1_train = nn.TrainOneStepCell(DisWithLossCell1(generator, discriminator1,Dnet), optimizerD1)

netG_train.set_train()
netD_train.set_train()
netD1_train.set_train()
## Data pipeline
transforms_ = Compose([
            vision.Resize((img_height, img_width)),
            vision.ToTensor(),
            vision.Normalize([0.5, 0.5, 0.5],
                            [0.5, 0.5, 0.5],is_hwc=False)])


train_iterator, iterations_epoch, data_loader = get_train_loader(
    dataset_path,
    batch_size,
    transforms_=transforms_,
    shuffle=True,
    num_workers=1,
)

## Training pipeline
lossD = []
lossD1 = []
lossG = []
i = 1

Dnet.set_train(False)
coenet.set_train(False)
Ttodnet.set_train(False)
for epoch in range(from_epoch, num_epochs):
    for i, batch in enumerate(train_iterator):
        # Model inputs
        imgs_distorted = Tensor(batch["A"]).squeeze(axis=1)
        imgs_good_gt = Tensor(batch["B"]).squeeze(axis=1)

        depth = Dnet(imgs_distorted)
        ae = coenet(imgs_distorted)
        tm = Ttodnet(ae, depth)[0]
        
        loss_D = netD_train(imgs_distorted, tm,imgs_good_gt)
        lossD.append(float(loss_D) / (len(imgs_distorted)))

        ## Train Discriminator
        loss_D1 = netD1_train(imgs_distorted, tm, imgs_good_gt)
        lossD.append(float(loss_D1) / (len(imgs_distorted)))

        ## Train Generator at 1:num_critic rate
        if i % num_critic == 0:
  
            depth = Dnet(imgs_distorted)
            ae = coenet(imgs_distorted)
            tm = Ttodnet(ae, depth)[0]
            loss_G = netG_train(imgs_distorted, tm,imgs_good_gt)
            lossG.append(float(loss_G) / (len(imgs_distorted)))

        ## Print log
        if not i % 50:
            sys.stdout.write("\r[Epoch %d/%d: batch %d/%d] [DLoss: %.3f, D1Loss: %.3f, GLoss: %.3f] "
                             % (epoch, num_epochs, i, iterations_epoch,
                                 loss_D, loss_D1, loss_G ))

    ## Save model checkpoints
    if ((epoch + 1) % ckpt_interval == 0):
        ms.save_checkpoint(generator,"./checkpoints/UIEB/ugan_generator_" + str(version) + str(epoch) + ".ckpt")
        ms.save_checkpoint(discriminator,"./checkpoints/UIEB/ugan_discriminator_" + str(version) + str(epoch) + ".ckpt")
        ms.save_checkpoint(discriminator1,"./checkpoints/UIEB/ugan_discriminator1_" + str(version) + str(epoch) + ".ckpt")
