import os
import time
import argparse
import yaml
import numpy as np
from PIL import Image
from glob import glob
from ntpath import basename
from os.path import join, exists
from Par.model import DNet, TtoDNet, AENet
from mindspore import Tensor
import mindspore as ms
from mindspore import nn, dataset, context, ops
from mindspore.ops import functional as F
import mindspore.dataset.vision as vision
from mindspore.dataset.transforms import Compose
import cv2

## options
parser = argparse.ArgumentParser()
parser.add_argument("--cfg_file", type=str, default="test.yaml")
args = parser.parse_args()

with open(args.cfg_file) as f:
    cfg = yaml.load(f, Loader=yaml.FullLoader)
# get info from config file
dataset_path = cfg["data_dir"]
sample_path = cfg["sample_dir"]
gtr_path = cfg["gtr_dir"]
img_width = cfg["im_width"]
img_height = cfg["im_height"]
channels = cfg["chans"]
model_path = cfg["path_gen"]
model_path_d = cfg["path_d"]
model_path_t = cfg["path_t"]
model_path_a = cfg["path_a"]

## checks
assert exists(model_path), "Generator model not found"
assert exists(model_path_a), "AENet model not found"
assert exists(model_path_d), "DNet model not found"
assert exists(model_path_t), "TNet model not found"

## model arch

from nets.fusion import PUGAN
model = PUGAN().netG
Dnet = DNet()
Ttodnet = TtoDNet()
coenet = AENet()
model.set_train(False)
Dnet.set_train(False)
Ttodnet.set_train(False)
coenet.set_train(False)
## load weights
model_dict = ms.load_checkpoint(model_path)
ms.load_param_into_net(model, model_dict)
ms.load_param_into_net(Dnet, ms.load_checkpoint(model_path_d))
ms.load_param_into_net(Ttodnet, ms.load_checkpoint(model_path_t))
ms.load_param_into_net(coenet, ms.load_checkpoint(model_path_a))
## data pipeline
transforms_ = [vision.Resize((img_height, img_width)),
               vision.ToTensor(),
                vision.Normalize([0.5, 0.5, 0.5],
                            [0.5, 0.5, 0.5],is_hwc=False)]
transform = Compose(transforms_)


def unload(x):
    y = x.squeeze().asnumpy()
    return y


def min_max_normalization(x):
    x_normed = (x - np.min(x)) / (np.max(x)-np.min(x))
    return x_normed


def convert2img(x):
    return Image.fromarray(x*255).convert('L')


def save_smap(smap, path, negative_threshold=0.25):

    smap = convert2img(smap)
    smap.save(path)

def decode_image(imgAfterInfer):
    """ Decode Image """
    mean = 0.5 * 255
    std = 0.5 * 255
    return (imgAfterInfer * std + mean).astype(np.uint8).transpose((1, 2, 0))

## testing loop
times = []
test_files = sorted(glob(join(dataset_path, "*.*")))
for path in test_files:
    inp_img = transform(Image.open(path))
    inp_img = ms.Tensor(inp_img, ms.float32)
    depth = Dnet(inp_img)
    ae = coenet(inp_img)
    tm = Ttodnet(ae, depth)[0]
    s = time.time()
    gen_img = model(inp_img, tm)
    times.append(time.time()-s)

    gen_img = gen_img.squeeze(0)*255
    img_pil = gen_img.asnumpy().astype(np.uint8).transpose((1, 2, 0))
    img_pil = Image.fromarray(img_pil)
    img_pil.save(join(sample_path, basename(path)))

    print ("Tested: %s" % path)

## run-time    
if (len(times) > 1):
    print ("\nTotal samples: %d" % len(test_files))
    Ttime, Mtime = np.sum(times[1:]), np.mean(times[1:]) 
    print ("Time taken: %d sec at %0.3f fps" %(Ttime, 1./Mtime))
    print("Saved generated images in in %s\n" %(sample_path))

with open("./evaluations/measure.txt",'a+') as f:
    f.write('test on ' + dataset_path + 'use ' + model_path + ':')
    f.write('\n')
    print("end testing")
f.close()

