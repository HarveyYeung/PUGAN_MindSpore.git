import os
import xlrd
import time
import random
import imageio
import numpy as np
from math import exp
from PIL import Image
# import skimage.transform
import matplotlib.pyplot as plt
from model import AENet
from PIL import Image
import mindspore as ms
from mindspore import nn, dataset, context, ops
from mindspore.ops import functional as F
os.environ['CUDA_VISIBLE_DEVICES']='0'

#resize图像  112*112
def read_and_resize(paths, res=(112, 112), mode_='RGB'):
    equi_img = Image.open(paths).convert('RGB')
    # img = imageio.imread(paths, pilmode=mode_).astype(np.float32)
    # img = skimage.transform.resize(img, res)
    img = equi_img.resize(res, resample=Image.LANCZOS)
    return img
#正则化
def noramlization(x):
    argmin = ops.ArgMinWithValue()
    argmax =  ops.ArgMaxWithValue()
    minVals = argmin(x)
    maxVals = argmax(x)
    ranges = maxVals - minVals
    x=(x-minVals)/ranges
    return x
#归一化
def prenarrow(x):
    return (x/255)


class loaddata_test():
    def __init__(self, path):
        self.imgsize = (256, 256)
        self.folder = "data_coe/"
        self.get_paths(path)

    def get_paths(self, path):
        self.num_train, self.num_val = 0, 0
        self.train_paths, self.val_paths = [], []
        self.gt_tr_paths, self.gt_val_paths = [], []
        data_dir = os.path.join(path, self.folder)
        data_path = sorted(os.listdir(data_dir))
        num_paths = max(len(data_path), 0)
        all_idx = list(range(num_paths))
        # 95% train-val splits
        random.seed(2)
        random.shuffle(all_idx)
        self.num_val = 12
        self.num_train = num_paths - self.num_val
        train_idx = set(all_idx[:self.num_train])
        # split data paths to training and validation sets
        for i in range(num_paths):
            if i in train_idx:
                self.train_paths.append(data_dir + str(i+1) + ".jpg")
                self.gt_tr_paths.append(str(i+1))
            else:
                self.val_paths.append(data_dir + str(i) + ".jpg")
                self.gt_val_paths.append(str(i))
        print("Loaded {0} samples for training".format(self.num_train))

    def get_path(self, index):
        return self.train_paths[index], self.gt_tr_paths[index]

# 将训练集集路径转为tensor的加载器
class Dataset_test_tr(object):
    def __init__(self, path):
        self.load = loaddata_test(path)
        self.resimg = (256, 256)
        self.le = 0
        self.le = len(self.load.train_paths)
        xls_file = xlrd.open_workbook(path + "coe.xls")
        self.xls_sheet = xls_file.sheets()[0]

    # 读取img  转为tensor   读取lable  转为tensor
    def __getitem__(self, index):
        path = self.load.train_paths[index]
        pid = self.load.gt_tr_paths[index]
        img = read_and_resize(path, res=self.resimg)
        row_value = self.xls_sheet.row_values(int(pid) - 1)
        imgs_tensor = np.transpose(prenarrow(np.array(img)), (2, 0, 1))
        imgs_label = row_value
        return imgs_tensor, imgs_label

    def __len__(self):
        return self.le

loss_=nn.L1Loss()


class ComputeLoss(nn.Cell):
    def __init__(self, network, loss_fn):
        super(ComputeLoss, self).__init__(auto_prefix=False)
        self.network = network
        self._loss_fn = loss_fn
    def construct(self, batch_x, batch_y):
        x = self.network(batch_x)
        loss= self._loss_fn(x, batch_y)
        return loss
def train_coe(epoch_num=41):
    train_data=Dataset_test_tr("../data/coe_data/")
    batch_size=4
    # train_loader = DataLoader(dataset=train_data, batch_size=4, shuffle=True,drop_last=False)
    data_loader = dataset.GeneratorDataset(
        train_data, ["imgs_tensor", "imgs_label"], shuffle=True, num_parallel_workers=4)  
    data_loader = data_loader.batch(batch_size)
    iterations_epoch = data_loader.get_dataset_size()
    train_iterator = data_loader.create_dict_iterator()   
    ms.set_context(device_target="GPU")
    coe_model=AENet()
    print("COE_model loaded")
    optimizer = nn.optim.Adam(coe_model.trainable_params())
    bce_loss = nn.L1Loss()
    model = ComputeLoss(coe_model,bce_loss)
    T_net = nn.TrainOneStepCell(model, optimizer)
    coe_model.set_train()
    print("Optimizer loaded")
    Loss_train = []
    for epoch in range(epoch_num):
        time_start=time.time()
        print('epoch {}'.format(epoch + 1))
        # training-----------------------------
        train_loss = 0.
        for i, batch in enumerate(train_iterator):
            # Model inputs
            batch_x = ms.Tensor(batch["imgs_tensor"].astype(np.float32))
            batch_y = ms.Tensor(batch["imgs_label"].astype(np.float32))
            loss = T_net(batch_x,batch_y)
            #print(out)
            train_loss+=loss
        Loss_train.append(train_loss / (len(train_data))*batch_size)
        print('Train Loss: {:.6f}'.format(float(train_loss / (len(train_data)))))
        time_end=time.time()
        print('totally cost',time_end-time_start)
        if (epoch+1)%20==0:
            ms.save_checkpoint(coe_model,"model/coe_"+str(epoch+1)+".ckpt")
    print("end")
    return Loss_train

import matplotlib.pyplot as plt
def drow():
    plt.xlabel("epoch")
    plt.ylabel("loss")
    plt.plot(Train_Loss, label ="Train Loss")
    plt.legend()
    plt.show()
    plt.savefig("coe_loss.png")

#开始训练并返回train和test损失
Train_Loss=train_coe()
drow()