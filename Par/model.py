import time
import random
import numpy as np
from math import exp, log
import matplotlib.pyplot as plt
from mindspore import nn
from mindspore.ops import operations as P
from mindspore.ops import functional as F

def expand(ae, depth):
    x1 = depth
    x2 = depth
    x3 = depth
    a = ae.shape
    i = a[0]
    cat1 = P.Concat(axis=1)
    for i in range(i):
        x1[i][0].mul(ae[i][0])
        x2[i][0].mul(ae[i][1])
        x3[i][0].mul(ae[i][2])
    x = cat1([x1, x2, x3])
    return x

class RES(nn.Cell):
    def __init__(self, in_ch, out_ch, k_size, pad):
        super(RES, self).__init__()
        self.conv = nn.Conv2d(in_ch, out_ch, kernel_size=k_size, padding=pad, pad_mode='pad', has_bias=False)
        self.re = nn.ReLU()
        self.bn = nn.BatchNorm2d(out_ch)
        self.cat1 = P.Concat(axis=1)
    def construct(self, x):
        y = self.conv(x)
        y = self.bn(y)
        y = self.re(y)
        return self.cat1([y, x])

class Net(nn.Cell):
    def __init__(self):
        super(Net, self).__init__()
        layers1 = [
            nn.Conv2d(3, 16, kernel_size=3, padding=1,pad_mode='pad', has_bias=False),
            nn.InstanceNorm2d(16),
            nn.ReLU(),
        ]
        layers2 = [
            nn.Conv2d(16, 32, kernel_size=3, padding=1,pad_mode='pad', has_bias=False),
            nn.InstanceNorm2d(32),
            nn.ReLU(),
        ]
        layers3 = [
            nn.Conv2d(32, 16, kernel_size=3, padding=1,pad_mode='pad', has_bias=False),
            nn.InstanceNorm2d(16),
            nn.ReLU(),
        ]
        layers4 = [
            nn.Conv2d(16, 3, kernel_size=3, padding=1,pad_mode='pad', has_bias=False),
            nn.InstanceNorm2d(3),
            nn.ReLU(),
        ]
        self.model1 = nn.SequentialCell(*layers1)
        self.model2 = nn.SequentialCell(*layers2)
        self.model3 = nn.SequentialCell(*layers3)
        self.model4 = nn.SequentialCell(*layers4)
    def construct(self, x):
        x = self.model4(self.model3(self.model2(self.model1(x))))
        return x

class preAE(nn.Cell):
    def __init__(self):
        super(preAE,self).__init__()
        self.conv_layer1=nn.Conv2d(1,32,kernel_size=3,padding=1,pad_mode='pad',has_bias=False)
        self.conv_layer2=nn.Conv2d(32,64,kernel_size=3,padding=1,pad_mode='pad',has_bias=False)
        self.conv_layer3=nn.Conv2d(64,32,kernel_size=3,padding=1,pad_mode='pad',has_bias=False)
        self.lin1=nn.Dense(32768,128)  #32*32*32 = 32768
        # self.pooling=nn.MaxPool2d(2,pad_mode="valid")
        self.pooling=  nn.MaxPool2d(kernel_size=2, stride=2)
        self.lin2=nn.Dense(128,1)
        self.re=nn.ReLU()
    def construct(self,x):
        batch_size=x.shape[0]
        # (1, 1, 256, 256)
        x=self.re(self.pooling(self.conv_layer1(x)))
        x=self.re(self.pooling(self.conv_layer2(x)))
        x=self.re(self.pooling(self.conv_layer3(x)))
        x=x.view(batch_size,-1)
        x=self.lin2(self.re(self.lin1(x)))
        return x
class AENet(nn.Cell):
    def __init__(self):
        super(AENet,self).__init__()
        self.preR=preAE()
        self.preG=preAE()
        self.preB=preAE()
        self.cat = P.Concat(axis=0)
        self.cat1 = P.Concat(axis=1)
        
    def construct(self,x):
        batch_size=x.shape[0]
        out=[]
        chunk0 = P.Split(axis=0, output_num=batch_size)
        X=chunk0(x)
        for i in range (batch_size):
            chunk1 = P.Split(axis=1, output_num=3)
            y=chunk1(X[i])
            xr=self.preR(y[0])
            xg=self.preG(y[1])
            xb=self.preB(y[2])
            out.append(self.cat1([xr,xg,xb]))
        return self.cat(out)

class DNet(nn.Cell):
    def __init__(self):
        super(DNet, self).__init__()
        self.net = Net()
        layers1 = [
            nn.Conv2d(3, 3, kernel_size=3, padding=1, pad_mode='pad', has_bias=False),
            nn.InstanceNorm2d(3),
            nn.ReLU(),
        ]
        layers2 = [
            nn.Conv2d(3, 1, kernel_size=3, padding=1, pad_mode='pad', has_bias=False),
            #nn.InstanceNorm2d(3),
            nn.Sigmoid(),
        ]
        self.model1 = nn.SequentialCell(*layers1)
        self.model2 = nn.SequentialCell(*layers2)
    def construct(self,x):
        x1 = self.net(x)
        depth = self.model2(self.model1(x1))
        return depth

class TNet(nn.Cell):
    def __init__(self):
        super(TNet, self).__init__()
        self.res_1 = RES(4, 4, 3, 1)
        self.conv1x3 = nn.Conv2d(8, 1, kernel_size=1)
        self.bn = nn.BatchNorm2d(1)
        self.cat = P.Concat(axis=1)
    def construct(self, ae, depth):
        ae1 = expand(ae, depth)
        x = self.cat((depth,ae1))
        x1 = self.res_1(x)
        x4 = self.conv1x3(x1)
        return x4

class TtoDNet(nn.Cell):
    def __init__(self):
        super(TtoDNet, self).__init__()
        self.res_1 = RES(1, 3, 3, 1)
        self.res_2 = RES(4, 16, 3, 1)
        layers1 = [
            nn.Conv2d(20, 8, kernel_size=1),
            nn.InstanceNorm2d(8),
            nn.ReLU(),
        ]
        self.model1 = nn.SequentialCell(*layers1)
        layers2 = [
            nn.Conv2d(8, 1, kernel_size=1),
        ]
        self.model2 = nn.SequentialCell(*layers2)
        self.tnet = TNet()

    def construct(self,ae, depth):
        a = self.tnet(ae, depth)
        x = self.res_1(a)
        x = self.res_2(x)
        x4 = self.model2(self.model1(x))
        return a, x4


