import os
import time
import glob
import random
import imageio
import numpy as np
from math import exp
from PIL import Image
import skimage.transform
import matplotlib.pyplot as plt
from model import DNet, TNet, TtoDNet, AENet

import mindspore as ms
from mindspore import nn, dataset, context, ops
from mindspore.ops import functional as F
import mindspore.dataset.vision as vision
from mindspore.dataset.transforms import Compose
os.environ['CUDA_VISIBLE_DEVICES']='1'
#读取数据集
class GetTrainingPairs():
    """ Common data pipeline to organize and generate
         training pairs for various datasets
    """

    def __init__(self, root, transforms1_= None, transforms2_ = None):
        self.transform1 = transforms1_
        self.transform2 = transforms2_
        self.filesA, self.filesB = self.get_file_paths(root)
        self.len = min(len(self.filesA), len(self.filesB))

    def __getitem__(self, index):
        img_A = Image.open(self.filesA[index % self.len])
        img_B = Image.open(self.filesB[index % self.len])
        if np.random.random() < 0.5:
            img_A = Image.fromarray(np.array(img_A)[:, ::-1, :], "RGB")
            img_B = Image.fromarray(np.array(img_B))
        img_A = self.transform1(img_A)
        img_B = self.transform2(img_B)
        return img_A,  img_B

    def __len__(self):
        return self.len

    def get_file_paths(self, root):
        filesA = sorted(glob.glob(os.path.join(root, 'input_train') + "/*.*"))
        filesB = sorted(glob.glob(os.path.join(root, 'depth_train') + "/*.*"))

        return filesA, filesB

#退化图像
transforms1_ = Compose([
            vision.Resize((256, 256)),
            vision.ToTensor(),
            vision.Normalize([0.5, 0.5, 0.5],
                            [0.5, 0.5, 0.5],is_hwc=False)])
#深度图gt
transforms2_ = Compose([
            vision.Resize((256, 256)),
            vision.ToTensor(),
            vision.Normalize([0.5,],[0.5,],is_hwc=False)])

#深度图gt
batch_size = 2
dataset_generator = GetTrainingPairs('../data', transforms1_=transforms1_, transforms2_=transforms2_)
data_loader = dataset.GeneratorDataset(
                dataset_generator, ["A", "B"], shuffle=True, num_parallel_workers=8)        
data_loader = data_loader.batch(batch_size)
iterations_epoch = data_loader.get_dataset_size()
train_iterator = data_loader.create_dict_iterator()



#定义各种参数
num_epochs = 100
lr_rate = 0.001
L1_G1  = nn.L1Loss() # l1 loss term
L1_G2  = nn.MSELoss()
Dnet = DNet()
Ttodnet = TtoDNet()

optimizer_D = nn.optim.Adam(Dnet.trainable_params(),
                    lr_rate,
                    weight_decay=0.001)
optimizer_T = nn.optim.Adam(Ttodnet.trainable_params(),
                lr_rate,
                weight_decay=0.001)

coenet = AENet()
param_dict = ms.load_checkpoint('./model/coe_40.ckpt')
ms.load_param_into_net(coenet, param_dict)


class ComputeLossDnet(nn.Cell):
    def __init__(self, network, loss_fn1, loss_fn2):
        super(ComputeLossDnet, self).__init__(auto_prefix=False)
        self.network = network
        self._loss_fn1 = loss_fn1
        self._loss_fn2 = loss_fn2
    def construct(self, batch_x, batch_y):
        x = self.network(batch_x)
        loss1= self._loss_fn1(x,batch_y)
        loss2 = self._loss_fn2(x,batch_y)
        loss = 5 * loss1 + loss2
        return loss

class ComputeLossTtodnet(nn.Cell):
    def __init__(self, network, loss_fn1, loss_fn2):
        super(ComputeLossTtodnet, self).__init__(auto_prefix=False)
        self.network = network
        self._loss_fn1 = loss_fn1
        self._loss_fn2 = loss_fn2
    def construct(self, batch_x,batch_x2, batch_y):
        x = self.network(batch_x,batch_x2)
        loss1= self._loss_fn1(x[1], batch_y)
        loss2 = self._loss_fn2(x[1], batch_y)
        loss = 5 * loss1 + loss2
        return loss


class ComputeLossCombine(nn.Cell):
    def __init__(self, network1,network2, loss_fn1, loss_fn2):
        super(ComputeLossTtodnet, self).__init__(auto_prefix=False)
        self.network1 = network1
        self.network2 = network2
        self._loss_fn1 = loss_fn1
        self._loss_fn2 = loss_fn2

    def construct(self, batch1_x,batch2_x, batch_y):
        x = self.network1(batch_x)
        loss1_1= self._loss_fn1(x,batch_y)
        loss1_2 = self._loss_fn2(x,batch_y)
        loss1 = 5 * loss1_1 + loss1_2
        x2 = self.network2(batch2_x, ops.stop_gradient(x))
        loss2_1= self._loss_fn1(x2[1], batch_y)
        loss2_2 = self._loss_fn2(x2[1], batch_y)
        loss2 = 5 * loss2_1 + loss2_2
        return loss1, loss2

class MultiLossTrainOneStepCell(nn.TrainOneStepCell):
    def __init__(self, network, optimizer, sens=1.0):
        super(MultiLossTrainOneStepCell, self).__init__(network, optimizer, sens)

    def construct(self, *inputs):
        loss1,loss2 = self.network(*inputs)
        sens1 = F.fill(loss1.dtype, loss1.shape, self.sens)
        sens2 = F.fill(loss2.dtype, loss2.shape, self.sens)
        sens = (sens1,sens2)
        grads = self.grad(self.network, self.weights)(*inputs, sens)
        grads = self.grad_reducer(grads)
        return F.depend(loss1, self.optimizer(grads)),F.depend(loss2, self.optimizer(grads))


def train_main(epoch=0):
    print("depth_model loaded")
    print("OptimizerD loaded")
    Loss_depth = []
    Loss_depth1 = []

    Dnetmodel = ComputeLossDnet(Dnet,L1_G1,L1_G2)
    T_Dnet = nn.TrainOneStepCell(Dnetmodel, optimizer_D)

    Ttodnetmodel = ComputeLossTtodnet(Ttodnet,L1_G1,L1_G2)
    T_Ttodnet = nn.TrainOneStepCell(Ttodnetmodel, optimizer_T)
    Dnet.set_train()
    Ttodnet.set_train()
    for epoch in range(epoch, num_epochs):
        print('epoch {}'.format(epoch + 1))
        time_start = time.time()

        loss_depth = 0.
        loss_depth1 = 0.
        N = 0.
        for i, batch in enumerate(train_iterator):
            batch_x = ms.Tensor(batch["A"]).squeeze(axis=1)
            batch_y = ms.Tensor(batch["B"]).squeeze(axis=1)
            B = batch_x.shape[0]
            loss12 = T_Dnet(batch_x,batch_y)
            N += B
            loss_depth = loss_depth + loss12
            x = Dnet(batch_x)
            ae = coenet(batch_x)

            loss45 = T_Ttodnet(ae, ops.stop_gradient(x),batch_y)
            loss_depth1 = loss_depth1 + loss45

        Loss_depth.append(loss_depth / N*(len(batch_x)))
        Loss_depth1.append(loss_depth1 / N * (len(batch_x)))
        print('Depth1 Loss: {:.6f}'.format(float(loss_depth / N*len(batch_x))))
        print('Depth2 Loss: {:.6f}'.format(float(loss_depth1 / N * len(batch_x))))

        time_end = time.time()
        print('totally cost', time_end - time_start)
        if (epoch + 1) % 5 == 0:
            ms.save_checkpoint(Dnet,"./model/Dnet_" + str(epoch + 1) + ".ckpt")
            ms.save_checkpoint(Ttodnet,"./model/Tnet_" + str(epoch + 1) + ".ckpt")
    print("end")
    return Loss_depth,Loss_depth1



import matplotlib.pyplot as plt
def drow_depth():
    plt.xlabel("epoch")
    plt.ylabel("loss")
    plt.plot(depth, label="Train depth")
    plt.plot(depth2, label="Train depth1")
    plt.legend()
    plt.show()
    plt.savefig("loss_depth.png")


def drow_ae():
    plt.xlabel("epoch")
    plt.ylabel("loss")
    plt.plot(Lossae, label="train ae")
    plt.legend()
    plt.show()
    plt.savefig("loss_ae.png")

depth, depth2 = train_main()
drow_depth()
print("ok")


